create table product (
                       id serial primary key,
                       description varchar(100),
                       price decimal(8, 2),
                       amount integer check ( amount >= 0 )
);

create table client (
                          id serial primary key,
                          firstName varchar(20),
                          lastName varchar(20)
);
CREATE TABLE orders (
                       id serial primary key,
                       product_id integer,
                       foreign key (product_id) references product(id),
                       client_id integer,
                       foreign key (client_id) references client(id),
                       date DATE,
                       amount integer check ( amount >= 0 )
);
insert into product (description, price, amount)
values ('Ходовые огни Fish eye 10W 12-24V W (к-т 2 шт) V0114', 600.00, 5);

insert into product (description, price, amount)
values ('Резистор ( CANBUS ) 50W 12V (комплект 2 шт.)', 230.00, 3);

insert into product (description, price, amount)
values ('Резистор ( CANBUS ) 25W 12V (комплект 2 шт.)', 200.00, 10);

insert into product (description, price, amount)
values ('V0237 Комплект светодиодных ламп с обманкой 1156-2835-42SMD 12V W+Y (P21W-P21/5w)', 800.00, 2);

insert into product (description, price, amount)
values ('V0240 Комплект светодиодных ламп с обманкой 1156-2835-20SMD 12V ', 780.00, 4);

insert into product (description, price, amount)
values ('Разъём пластиковый для лампы H1', 14.00, 25);

insert into product (description, price, amount)
values ('Патрон пластиковый для лампы T10', 17.00, 20);

insert into product (description, price, amount)
values ('H050 CZH-NIVA-02 Надфарник светодиодный (компл 2шт)', 2150.00, 1);

insert into product (description, price, amount)
values ('H054 CZH-NIVA-07 Повторитель поворота светодиодный (компл 2шт)', 725.00, 1);

insert into product (description, price, amount)
values ('H055 CZH-NIVA-08 Задние фонари светодиодные (компл-2шт)', 4600.00, 1);

insert into client (firstName, lastName)
values ('Екатерина', 'Собчук');

insert into client (firstName, lastName)
values ('Сергей', 'Чернов');

insert into client (firstName, lastName)
values ('Ольга', 'Рубцова');

insert into orders (product_id, client_id, date, amount)
values (1, 1, '2021-12-09', 1);

insert into orders (product_id, client_id, date, amount)
values (4, 3, '2021-11-09', 1);

insert into orders (product_id, client_id, date, amount)
values (6, 2, '2021-12-01', 10);

