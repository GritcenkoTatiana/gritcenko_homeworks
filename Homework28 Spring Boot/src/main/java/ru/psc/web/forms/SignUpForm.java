package ru.psc.web.forms;

import lombok.Data;

@Data
public class SignUpForm {

    private String firstname;
    private String lastname;
    private String email;
    private String password;

}
