package ru.psc.web.forms;

import lombok.Data;

@Data
public class ClientForm {
    private String firstname;
    private String lastname;
}