package ru.psc.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.psc.web.exceptions.UserNotFoundException;
import ru.psc.web.forms.ClientForm;
import ru.psc.web.forms.ProductForm;
import ru.psc.web.models.Client;
import ru.psc.web.models.Order;
import ru.psc.web.models.Product;
import ru.psc.web.repositories.ClientRepository;
import ru.psc.web.repositories.OrdersRepository;
import ru.psc.web.repositories.ProductRepository;

import java.util.List;

@RequiredArgsConstructor
@Component
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final OrdersRepository ordersRepository;
    private final ClientRepository clientRepository;

    /*@Autowired
    public ProductServiceImpl(ProductRepository productRepository, OrdersRepository ordersRepository) {
        this.productRepository = productRepository;
        this.ordersRepository = ordersRepository;
    }*/
    @Override
    public void addProduct(ProductForm form){
        Product product = Product.builder()
                .description(form.getDescription())
                .price(form.getPrice())
                .amount(form.getAmount())
                .build();
        productRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public List<Order> getAllOrders() {
        return ordersRepository.findAll();
    }

    @Override
    public void deleteProduct(Integer productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public Product getProduct(Integer productId) {
        return productRepository.getById(productId);
    }

    @Override
    public void update(Integer productId, ProductForm form) {
        Product product = productRepository.getById(productId);
        product.setDescription(form.getDescription());
        product.setPrice(form.getPrice());
        product.setAmount(form.getAmount());
        productRepository.save(product);
    }

    @Override
    public List<Order> getOrdersByProduct (Integer productId) {
        return ordersRepository.findAllByProduct_Id(productId);
    }

    @Override
    public List<Order> getOrdersByClient(Integer clientId) {
        return ordersRepository.findAllByClient_id(clientId);
    }

    @Override
    public void deleteClient(Integer clientId) {
        clientRepository.deleteById(clientId);
    }

    @Override
    public Client getClient(Integer clientId) {
        return clientRepository.findById(clientId).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void updateClient(Integer clientId, ClientForm form) {
        Client client = clientRepository.getById(clientId);
        client.setFirstname(form.getFirstname());
        client.setLastname(form.getLastname());
        clientRepository.save(client);
    }

    /*@Override
    public void addClient(ClientForm form) {
        Client client = Client.builder()
                .firstname(form.getFirstname())
                .lastname(form.getLastname())
                .build();
        clientRepository.save(client);

    }*/

}
