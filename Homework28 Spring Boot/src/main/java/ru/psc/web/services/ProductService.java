package ru.psc.web.services;

import ru.psc.web.forms.ClientForm;
import ru.psc.web.forms.ProductForm;
import ru.psc.web.models.Client;
import ru.psc.web.models.Order;
import ru.psc.web.models.Product;

import java.util.List;

public interface ProductService {
    void addProduct(ProductForm form);
    List<Product> getAllProducts();
    List<Client> getAllClients();
    List<Order> getAllOrders();

    void deleteProduct(Integer productId);

    Product getProduct(Integer productId);

    void update(Integer productId, ProductForm form);

    List<Order> getOrdersByProduct (Integer productId);

    List<Order> getOrdersByClient(Integer clientId);

    void deleteClient(Integer clientId);

    Client getClient(Integer clientId);

    void updateClient(Integer clientId, ClientForm form);

    /*void addClient(ClientForm form);*/
}