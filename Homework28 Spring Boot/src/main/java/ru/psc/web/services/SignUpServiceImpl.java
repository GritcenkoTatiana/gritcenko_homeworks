package ru.psc.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.psc.web.forms.SignUpForm;
import ru.psc.web.models.Client;
import ru.psc.web.repositories.ClientRepository;

@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final ClientRepository clientRepository;

    @Override
    public void signUpClient(SignUpForm form) {
        Client client = Client.builder()
                .firstname(form.getFirstname())
                .lastname(form.getLastname())
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(Client.Role.CLIENT)
                .build();
        clientRepository.save(client);

    }
}
