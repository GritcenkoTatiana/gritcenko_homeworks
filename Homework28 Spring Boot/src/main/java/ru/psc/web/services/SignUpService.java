package ru.psc.web.services;

import ru.psc.web.forms.SignUpForm;

public interface SignUpService {
    void signUpClient(SignUpForm form);
}
