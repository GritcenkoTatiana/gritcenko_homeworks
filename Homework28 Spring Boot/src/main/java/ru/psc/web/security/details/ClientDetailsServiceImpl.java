package ru.psc.web.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.psc.web.models.Client;
import ru.psc.web.repositories.ClientRepository;
@RequiredArgsConstructor
@Component
public class ClientDetailsServiceImpl implements UserDetailsService {

    private final ClientRepository clientRepository;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Client client = clientRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Client not found"));
        return new ClientDetailsImpl(client);
    }
}
