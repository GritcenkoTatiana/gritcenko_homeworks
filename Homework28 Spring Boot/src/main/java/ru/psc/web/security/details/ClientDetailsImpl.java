package ru.psc.web.security.details;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.psc.web.models.Client;

import java.util.Collection;
import java.util.Collections;

import static java.util.Collection.*;

public class ClientDetailsImpl implements UserDetails{

    private final Client client;

    public ClientDetailsImpl (Client client) {
        this.client = client;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = client.getRole().toString();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
        return client.getHashPassword();
    }

    @Override
    public String getUsername() {
        return client.getEmail();
    }
//Аккаунт не просрочен?
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
// Аккаунт не заблокирован?
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
// Пароль не просрочен?
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
// Аккаунт активный?
    @Override
    public boolean isEnabled() {
        return true;
    }
}
