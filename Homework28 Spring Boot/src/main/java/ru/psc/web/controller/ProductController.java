package ru.psc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.psc.web.forms.ClientForm;
import ru.psc.web.forms.ProductForm;
import ru.psc.web.models.Client;
import ru.psc.web.models.Order;
import ru.psc.web.models.Product;
import ru.psc.web.services.ProductService;

import java.util.List;

@Controller
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public String getProductsPage(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Integer productId) {
        Product product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @PostMapping("/products")
    public String addProduct(ProductForm form){
        productService.addProduct(form);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId) {
        productService.deleteProduct(productId);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/update")
    public String update(@PathVariable("product-id") Integer productId, ProductForm form) {
        productService.update(productId, form);
        return "redirect:/products";
    }

    @GetMapping("/products/{product-id}/orders")
    public String getOrdersByProduct(Model model, @PathVariable("product-id") Integer productId) {
        List<Order> orders = productService.getOrdersByProduct(productId);
        model.addAttribute("orders", orders);
        return "orders_of_product";
    }

    @GetMapping("/clients")
    public String getClientsPage(Model model) {
        List<Client> clients = productService.getAllClients();
        model.addAttribute("clients", clients);
        return "clients";
    }

    @GetMapping("/clients/{client-id}")
    public String getClientPage(Model model, @PathVariable("client-id") Integer clientId) {
        Client client = productService.getClient(clientId);
        model.addAttribute("client", client);
        return "client";
    }

    /*@PostMapping("/clients")
    public String addClient(ClientForm form){
        productService.addClient(form);
        return "redirect:/clients";
    }*/

    @PostMapping("/clients/{client-id}/updateClient")
    public String updateClient(@PathVariable("client-id") Integer clientId, ClientForm form) {
        productService.updateClient(clientId, form);
        return "redirect:/clients";
    }

    @GetMapping("/clients/{client-id}/orders")
    public String getOrdersByClient(Model model, @PathVariable("client-id") Integer clientId) {
        List<Order> orders = productService.getOrdersByClient(clientId);
        model.addAttribute("orders", orders);
        return "orders_of_client";
    }

    @PostMapping("/clients/{client-id}/delete")
    public String deleteClient(@PathVariable("client-id") Integer clientId) {
        productService.deleteClient(clientId);
        return "redirect:/clients";
    }

    @GetMapping("/orders")
    public String getOrdersPage(Model model) {
        List<Order> orders = productService.getAllOrders();
        model.addAttribute("orders", orders);
        return "orders";
    }
    @GetMapping("/index")
    public String getIndexPage() {
        return "index";
    }
}
