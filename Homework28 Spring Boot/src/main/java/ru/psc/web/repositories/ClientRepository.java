package ru.psc.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.psc.web.models.Client;
import ru.psc.web.models.Product;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Integer> {

    Optional<Client> findByEmail(String email);
}
