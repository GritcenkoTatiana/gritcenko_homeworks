package ru.psc.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.psc.web.models.Order;
import ru.psc.web.models.Product;

import java.util.List;

public interface OrdersRepository extends JpaRepository<Order, Integer> {

    List<Order> findAllByProduct_Id(Integer id);

    List<Order> findAllByClient_id(Integer clientId);
}
