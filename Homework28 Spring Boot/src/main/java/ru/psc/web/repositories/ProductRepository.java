package ru.psc.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.psc.web.models.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
}
