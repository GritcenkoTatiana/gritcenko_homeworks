/*
package ru.psc.web.repositories.old;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.psc.web.models.Product;

import javax.sql.DataSource;
import java.util.List;
@Component
public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {


    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from Товар where id = ?";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from Товар order by id";
    //language=SQL
    private static final String SQL_SELECT_BY_PRICE = "select * from Товар where стоимость = ?";
    //language=SQL
    private static final String SQL_INSERT = "insert into Товар (описание, стоимость, количество) values(?, ?, ?)";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from Товар where id = ?";
    //language=SQL

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        Long id = row.getLong("id");
        String description = row.getString("описание");
        double price = row.getDouble("стоимость");
        int amount = row.getInt("количество");
        return new Product(id, description, price, amount);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, productRowMapper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return null;
    }

    @Override
    public void delete(long productId) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, productId);
    }

    @Override
    public Product findById(Long productId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, productRowMapper, productId);
    }

    @Override
    public void save (Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getAmount());
    }
}
*/
