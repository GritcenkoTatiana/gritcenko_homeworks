# Учебный проект - интернет-магазин

## В проекте использован следующий стек технологий:

* Java Core (Collection API)
* PostgreSQL
* FreeMarker
* Maven
* Lombok
* Spring & Spring Boot
* Spring MVC
* ORM
* Hibernate & JPA
* Spring Data JPA
* Spring Security
